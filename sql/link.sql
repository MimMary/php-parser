-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 17, 2021 at 09:08 AM
-- Server version: 10.3.22-MariaDB
-- PHP Version: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `php-parser`
--

-- --------------------------------------------------------

--
-- Table structure for table `link`
--

CREATE TABLE `link` (
  `id` int(11) NOT NULL,
  `referrer` varchar(50) DEFAULT NULL,
  `href` varchar(50) DEFAULT NULL,
  `content` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `link`
--

INSERT INTO `link` (`id`, `referrer`, `href`, `content`, `created_at`) VALUES
(1, '', 'https://aparg.com/', 'https://aparg.com/', '2021-05-14 17:05:01'),
(2, 'https://aparg.com/', 'https://aparg.com/parg-co-url-shortener/', 'https://aparg.com/', '2021-05-14 17:05:03'),
(3, 'https://aparg.com/parg-co-url-shortener/', 'https://aparg.com/all-services/', 'https://aparg.com/', '2021-05-14 17:05:04'),
(4, 'https://aparg.com/all-portfolio/', 'https://aparg.com/all-services/hosting/', 'https://aparg.com/', '2021-05-14 17:05:04'),
(5, 'https://aparg.com/all-services/', 'https://aparg.com/all-news/', 'https://aparg.com/', '2021-05-14 17:05:05'),
(6, 'https://aparg.com/all-services/hosting/', 'https://aparg.com/blog/', 'https://aparg.com/', '2021-05-14 17:05:05'),
(7, 'https://aparg.com/all-news/', 'https://aparg.com/contacts/', 'https://aparg.com/', '2021-05-14 17:05:06'),
(8, 'https://aparg.com/contacts/', 'https://aparg.com/blog/wordpress/', 'https://aparg.com/', '2021-05-14 17:05:07'),
(9, 'https://aparg.com/tag/smartsocial/', 'https://aparg.com/tag/social/', 'https://aparg.com/', '2021-05-14 17:05:19'),
(10, 'https://aparg.com/all-careers/', 'https://aparg.com/careers/', 'https://aparg.com/', '2021-05-14 17:05:30'),
(11, 'https://aparg.com/careers/qa-engineer/', 'https://aparg.com/careers/react-native-developer/', 'https://aparg.com/', '2021-05-14 17:05:32'),
(12, 'https://aparg.com/careers/', 'https://aparg.com/tag/developer/', 'https://aparg.com/', '2021-05-14 17:05:32'),
(13, 'https://aparg.com/careers/react-native-developer/', 'https://aparg.com/careers/full-stack-developer/', 'https://aparg.com/', '2021-05-14 17:05:33'),
(14, 'https://aparg.com/tag/developer/', 'https://aparg.com/tag/hr/', 'https://aparg.com/', '2021-05-14 17:05:33'),
(15, 'https://aparg.com/tag/money/', 'https://aparg.com/tag/affiliate/', 'https://aparg.com/', '2021-05-14 17:05:40'),
(16, 'https://aparg.com/tag/affiliate/', 'https://aparg.com/tag/marketing/', 'https://aparg.com/', '2021-05-14 17:05:41'),
(17, 'https://aparg.com/tag/marketing/', 'https://aparg.com/tag/animation/', 'https://aparg.com/', '2021-05-14 17:05:42'),
(18, 'https://aparg.com/tag/adsence/', 'https://aparg.com/tag/adsence-alternatives/', 'https://aparg.com/', '2021-05-14 17:05:44'),
(19, 'https://aparg.com/tag/adsence-alternatives/', 'https://aparg.com/news/interview-about-velopark/', 'https://aparg.com/', '2021-05-14 17:05:44'),
(20, 'https://aparg.com/tag/android/', 'https://aparg.com/news/', 'https://aparg.com/', '2021-05-14 17:05:45'),
(21, 'https://aparg.com/news/interview-about-velopark/', 'https://aparg.com/news/ipsm-io-on-product-hunt/', 'https://aparg.com/', '2021-05-14 17:05:47'),
(22, 'https://aparg.com/news/', 'https://aparg.com/tag/ipsm-io/', 'https://aparg.com/', '2021-05-14 17:05:47'),
(23, 'https://aparg.com/news/ipsm-io-on-product-hunt/', 'https://aparg.com/tag/aparg/', 'https://aparg.com/', '2021-05-14 17:05:48'),
(24, 'https://aparg.com/tag/ipsm-io/', 'https://aparg.com/news/interview-about-aparg/', 'https://aparg.com/', '2021-05-14 17:05:48'),
(25, 'https://aparg.com/tag/aparg/', 'https://aparg.com/tag/interview/', 'https://aparg.com/', '2021-05-14 17:05:49'),
(26, 'https://aparg.com/news/interview-about-aparg/', 'https://aparg.com/tag/api/', 'https://aparg.com/', '2021-05-14 17:05:49'),
(27, 'https://aparg.com/tag/interview/', 'https://aparg.com/news/parg-co/', 'https://aparg.com/', '2021-05-14 17:05:50'),
(28, 'https://aparg.com/tag/api/', 'https://aparg.com/tag/chrome-extension/', 'https://aparg.com/', '2021-05-14 17:05:50'),
(29, 'https://aparg.com/news/parg-co/', 'https://aparg.com/tag/app/', 'https://aparg.com/', '2021-05-14 17:05:50'),
(30, 'https://aparg.com/tag/chrome-extension/', 'https://aparg.com/news/velopark-on-tv/', 'https://aparg.com/', '2021-05-14 17:05:51'),
(31, 'https://aparg.com/tag/app/', 'https://aparg.com/tag/ios/', 'https://aparg.com/', '2021-05-14 17:05:51'),
(32, 'https://aparg.com/news/velopark-on-tv/', 'https://aparg.com/news/itel-am-about-velopark/', 'https://aparg.com/', '2021-05-14 17:05:52'),
(33, 'https://aparg.com/tag/ios/', 'https://aparg.com/tag/media/', 'https://aparg.com/', '2021-05-14 17:05:52'),
(34, 'https://aparg.com/news/itel-am-about-velopark/', 'https://aparg.com/tag/banner/', 'https://aparg.com/', '2021-05-14 17:05:53'),
(35, 'https://aparg.com/tag/banner/', 'https://aparg.com/tag/tools/', 'https://aparg.com/', '2021-05-14 17:05:54'),
(36, 'https://aparg.com/tag/tools/', 'https://aparg.com/tag/parg-co/', 'https://aparg.com/', '2021-05-14 17:05:55'),
(37, 'https://aparg.com/news/parg-co-on-producthunt/', 'https://aparg.com/tag/bannersnack/', 'https://aparg.com/', '2021-05-14 17:05:55'),
(38, 'https://aparg.com/tag/bannersnack/', 'https://aparg.com/tag/animatron/', 'https://aparg.com/', '2021-05-14 17:05:56'),
(39, 'https://aparg.com/tag/animatron/', 'https://aparg.com/news/velopark/', 'https://aparg.com/', '2021-05-14 17:05:57'),
(40, 'https://aparg.com/tag/bike/', 'https://aparg.com/tag/phonegap/', 'https://aparg.com/', '2021-05-14 17:05:57'),
(41, 'https://aparg.com/news/velopark/', 'https://aparg.com/tag/cache/', 'https://aparg.com/', '2021-05-14 17:05:58'),
(42, 'https://aparg.com/tag/phonegap/', 'https://aparg.com/tag/cloud/', 'https://aparg.com/', '2021-05-14 17:05:58'),
(43, 'https://aparg.com/tag/cache/', 'https://aparg.com/news/aparg-hosting/', 'https://aparg.com/', '2021-05-14 17:05:59'),
(44, 'https://aparg.com/tag/cloud/', 'https://aparg.com/tag/cpanel/', 'https://aparg.com/', '2021-05-14 17:05:59'),
(45, 'https://aparg.com/news/aparg-hosting/', 'https://aparg.com/tag/codecanyon/', 'https://aparg.com/', '2021-05-14 17:06:00'),
(46, 'https://aparg.com/tag/codecanyon/', 'https://aparg.com/tag/envato/', 'https://aparg.com/', '2021-05-14 17:06:01'),
(47, 'https://aparg.com/tag/envato/', 'https://aparg.com/tag/plugin/', 'https://aparg.com/', '2021-05-14 17:06:02'),
(48, 'https://aparg.com/tag/plugin/', 'https://aparg.com/tag/stats/', 'https://aparg.com/', '2021-05-14 17:06:04'),
(49, 'https://aparg.com/tag/stats/', 'https://aparg.com/tag/statistics/', 'https://aparg.com/', '2021-05-14 17:06:05'),
(50, 'https://aparg.com/tag/statistics/', 'https://aparg.com/tag/eya16/', 'https://aparg.com/', '2021-05-14 17:06:07'),
(51, 'https://aparg.com/tag/eya16/', 'https://aparg.com/tag/velopark/', 'https://aparg.com/', '2021-05-14 17:06:08'),
(52, 'https://aparg.com/tag/velopark/', 'https://aparg.com/tag/optimization/', 'https://aparg.com/', '2021-05-14 17:06:13'),
(53, 'https://aparg.com/tag/hosting/', 'https://aparg.com/tag/performance/', 'https://aparg.com/', '2021-05-14 17:06:13'),
(54, 'https://aparg.com/tag/optimization/', 'https://aparg.com/tag/php/', 'https://aparg.com/', '2021-05-14 17:06:14'),
(55, 'https://aparg.com/tag/performance/', 'https://aparg.com/tag/producthunt/', 'https://aparg.com/', '2021-05-14 17:06:14'),
(56, 'https://aparg.com/tag/php/', 'https://aparg.com/tag/resize/', 'https://aparg.com/', '2021-05-14 17:06:15'),
(57, 'https://aparg.com/tag/producthunt/', 'https://aparg.com/news/aparg-watermark-resize/', 'https://aparg.com/', '2021-05-14 17:06:15'),
(58, 'https://aparg.com/news/aparg-watermark-resize/', 'https://aparg.com/about-us/', 'https://aparg.com/', '2021-05-14 17:06:18'),
(59, 'https://aparg.com/tag/resize/', 'https://aparg.com/tag/watermark/', 'https://aparg.com/', '2021-05-14 17:06:18'),
(60, 'https://aparg.com/news/aparg-watermark-resize/', 'https://aparg.com/tag/slider/', 'https://aparg.com/', '2021-05-14 17:06:19'),
(61, 'https://aparg.com/tag/watermark/', 'https://aparg.com/news/aparg-slider/', 'https://aparg.com/', '2021-05-14 17:06:19'),
(62, 'https://aparg.com/tag/slider/', 'https://aparg.com/aparg-slider-wordpress-plugin/', 'https://aparg.com/', '2021-05-14 17:06:20'),
(63, 'https://aparg.com/tag/slider/', 'https://aparg.com/tag/wordpress/', 'https://aparg.com/', '2021-05-14 17:06:24'),
(64, 'https://aparg.com/tag/wordpress/', 'https://aparg.com/tag/top-plugins/', 'https://aparg.com/', '2021-05-14 17:06:25'),
(65, 'https://aparg.com/tag/smartad/', 'https://aparg.com/tag/licensed-plugins%e2%80%8b/', 'https://aparg.com/', '2021-05-14 17:06:27'),
(66, 'https://aparg.com/tag/licensed-plugins%e2%80%8b/', 'https://aparg.com/tag/team/', 'https://aparg.com/', '2021-05-14 17:06:29'),
(67, 'https://aparg.com/tag/social-networks/', 'https://aparg.com/news/apargs-birthday-party/', 'https://aparg.com/', '2021-05-14 17:06:29'),
(68, 'https://aparg.com/tag/social-networks/', 'https://aparg.com/tag/tv/', 'https://aparg.com/', '2021-05-14 17:06:34'),
(69, 'https://aparg.com/tag/team/', 'https://aparg.com/tag/url-shortener/', 'https://aparg.com/', '2021-05-14 17:06:35'),
(70, 'https://aparg.com/tag/smartad/', 'https://aparg.com/tag/support/', 'https://aparg.com/', '2021-05-14 17:06:35'),
(71, 'https://aparg.com/tag/smartad/', 'https://aparg.com/tag/multi-language/', 'https://aparg.com/', '2021-05-14 17:06:39'),
(72, 'https://aparg.com/tag/smartad/', 'https://aparg.com/tag/qtranslate/', 'https://aparg.com/', '2021-05-14 17:06:39'),
(73, 'https://aparg.com/tag/smartad/', 'https://aparg.com/tag/qtranslate-x/', 'https://aparg.com/', '2021-05-14 17:06:40'),
(74, 'https://aparg.com/tag/smartad/', 'https://aparg.com/tag/wpml/', 'https://aparg.com/', '2021-05-14 17:06:41'),
(75, 'https://aparg.com/tag/smartad/', 'https://aparg.com/tag/top/', 'https://aparg.com/', '2021-05-14 17:06:45'),
(76, 'https://aparg.com/tag/smartad/', 'https://aparg.com/tag/adwords/', 'https://aparg.com/', '2021-05-14 17:06:48'),
(77, 'https://aparg.com/tag/smartad/', 'https://aparg.com/tag/google/', 'https://aparg.com/', '2021-05-14 17:06:48'),
(78, 'https://aparg.com/tag/plugin/', 'https://aparg.com/tag/strategy/', 'https://aparg.com/', '2021-05-14 17:06:49'),
(79, 'https://aparg.com/tag/bannersnack/', 'https://aparg.com/tag/html5maker/', 'https://aparg.com/', '2021-05-14 17:06:49'),
(80, 'https://aparg.com/tag/banner/', 'https://aparg.com/tag/vectr/', 'https://aparg.com/', '2021-05-14 17:06:50'),
(81, 'https://aparg.com/tag/adsence/', 'https://aparg.com/tag/pay-per-click/', 'https://aparg.com/', '2021-05-14 17:06:50'),
(82, 'https://aparg.com/tag/marketing/', 'https://aparg.com/tag/video/', 'https://aparg.com/', '2021-05-14 17:06:51'),
(83, 'https://aparg.com/tag/money/', 'https://aparg.com/tag/membership/', 'https://aparg.com/', '2021-05-14 17:06:51'),
(84, 'https://aparg.com/tag/money/', 'https://aparg.com/tag/woocomerce/', 'https://aparg.com/', '2021-05-14 17:06:52'),
(85, 'https://aparg.com/tag/ads/', 'https://aparg.com/tag/automate/', 'https://aparg.com/', '2021-05-14 17:06:53'),
(86, 'https://aparg.com/tag/ads/', 'https://aparg.com/tag/contact-form-7/', 'https://aparg.com/', '2021-05-14 17:06:53'),
(87, 'https://aparg.com/tag/ad/', 'https://aparg.com/tag/advertisement/', 'https://aparg.com/', '2021-05-14 17:06:55'),
(88, 'https://aparg.com/careers/', 'https://aparg.com/tag/react/', 'https://aparg.com/', '2021-05-14 17:07:03'),
(89, 'https://aparg.com/careers/', 'https://aparg.com/tag/react-native/', 'https://aparg.com/', '2021-05-14 17:07:03'),
(90, 'https://aparg.com/all-careers/', 'https://aparg.com/tag/qa/', 'https://aparg.com/', '2021-05-14 17:07:04'),
(91, 'https://aparg.com/blog/wordpress/', 'https://aparg.com/tag/content-locking/', 'https://aparg.com/', '2021-05-14 17:07:05'),
(92, 'https://aparg.com/blog/wordpress/', 'https://aparg.com/tag/hard-locking/', 'https://aparg.com/', '2021-05-14 17:07:06'),
(93, 'https://aparg.com/blog/wordpress/', 'https://aparg.com/tag/soft-locking/', 'https://aparg.com/', '2021-05-14 17:07:06'),
(94, 'https://aparg.com/contacts/', 'https://aparg.com/aparg-handygif-wordpress-plugin/', 'https://aparg.com/', '2021-05-14 17:07:07'),
(95, 'https://aparg.com/contacts/', 'https://aparg.com/tag/guide/', 'https://aparg.com/', '2021-05-14 17:07:09'),
(96, 'https://aparg.com/contacts/', 'https://aparg.com/tag/social-secrets/', 'https://aparg.com/', '2021-05-14 17:07:09'),
(97, 'https://aparg.com/all-news/', 'https://aparg.com/blog/page/2/', 'https://aparg.com/', '2021-05-14 17:07:10'),
(98, 'https://aparg.com/parg-co-url-shortener/', 'https://aparg.com/portfolio/awi-international/', 'https://aparg.com/', '2021-05-14 17:07:10'),
(99, 'https://aparg.com/all-portfolio/', 'https://aparg.com/portfolio/ani-grand-hotel/', 'https://aparg.com/', '2021-05-14 17:07:14'),
(100, 'https://aparg.com/portfolio/awi-international/', 'https://aparg.com/portfolio/ani-plaza-hotel/', 'https://aparg.com/', '2021-05-14 17:07:20'),
(101, 'https://aparg.com/portfolio/ani-grand-hotel/', 'https://aparg.com/portfolio/ani-central-inn/', 'https://aparg.com/', '2021-05-14 17:07:28'),
(102, 'https://aparg.com/portfolio/ani-plaza-hotel/', 'https://aparg.com/portfolio/dali-holding/', 'https://aparg.com/', '2021-05-14 17:07:35'),
(103, 'https://aparg.com/portfolio/ani-central-inn/', 'https://aparg.com/portfolio/aatt/', 'https://aparg.com/', '2021-05-14 17:07:38'),
(104, 'https://aparg.com/portfolio/dali-holding/', 'https://aparg.com/portfolio/greenway/', 'https://aparg.com/', '2021-05-14 17:07:42'),
(105, 'https://aparg.com/portfolio/aatt/', 'https://aparg.com/portfolio/easytraveling-app/', 'https://aparg.com/', '2021-05-14 17:07:46'),
(106, 'https://aparg.com/portfolio/easytraveling-app/', 'https://aparg.com/portfolio/lexpro/', 'https://aparg.com/', '2021-05-14 17:07:55'),
(107, 'https://aparg.com/portfolio/lexpro/', 'https://aparg.com/portfolio/reserve-am-app/', 'https://aparg.com/', '2021-05-14 17:08:04'),
(108, 'https://aparg.com/portfolio/transproject/', 'https://aparg.com/portfolio/aparg-smartsocial/', 'https://aparg.com/', '2021-05-14 17:08:08'),
(109, 'https://aparg.com/portfolio/reserve-am-app/', 'https://aparg.com/portfolio/phoenix-tour/', 'https://aparg.com/', '2021-05-14 17:08:18'),
(110, 'https://aparg.com/portfolio/aparg-smartsocial/', 'https://aparg.com/portfolio/diaserv/', 'https://aparg.com/', '2021-05-14 17:08:25'),
(111, 'https://aparg.com/portfolio/phoenix-tour/', 'https://aparg.com/portfolio/awi-redesign/', 'https://aparg.com/', '2021-05-14 17:08:29'),
(112, 'https://aparg.com/portfolio/diaserv/', 'https://aparg.com/portfolio/aparg-handygif/', 'https://aparg.com/', '2021-05-14 17:08:33'),
(113, 'https://aparg.com/portfolio/awi-redesign/', 'https://aparg.com/portfolio/gbakh-photography/', 'https://aparg.com/', '2021-05-14 17:08:36'),
(114, 'https://aparg.com/portfolio/aparg-handygif/', 'https://aparg.com/portfolio/visada-lifestyle/', 'https://aparg.com/', '2021-05-14 17:08:41'),
(115, 'https://aparg.com/portfolio/gbakh-photography/', 'https://aparg.com/portfolio/visada/', 'https://aparg.com/', '2021-05-14 17:08:46'),
(116, 'https://aparg.com/portfolio/visada-lifestyle/', 'https://aparg.com/portfolio/reserve/', 'https://aparg.com/', '2021-05-14 17:08:49'),
(117, 'https://aparg.com/portfolio/visada/', 'https://aparg.com/portfolio/miracle-house/', 'https://aparg.com/', '2021-05-14 17:08:52'),
(118, 'https://aparg.com/portfolio/reserve/', 'https://aparg.com/portfolio/releash/', 'https://aparg.com/', '2021-05-14 17:08:56'),
(119, 'https://aparg.com/portfolio/miracle-house/', 'https://aparg.com/portfolio/margarita-barkhoyan/', 'https://aparg.com/', '2021-05-14 17:08:59'),
(120, 'https://aparg.com/portfolio/releash/', 'https://aparg.com/portfolio/vardhrat/', 'https://aparg.com/', '2021-05-14 17:09:03'),
(121, 'https://aparg.com/portfolio/margarita-barkhoyan/', 'https://aparg.com/portfolio/carpark-app/', 'https://aparg.com/', '2021-05-14 17:09:05'),
(122, 'https://aparg.com/portfolio/vardhrat/', 'https://aparg.com/wp-content/uploads/2016/07/1.jpg', 'https://aparg.com/', '2021-05-14 17:09:06'),
(123, 'https://aparg.com/portfolio/vardhrat/', 'https://aparg.com/wp-content/uploads/2016/07/2.jpg', 'https://aparg.com/', '2021-05-14 17:09:06'),
(124, 'https://aparg.com/portfolio/vardhrat/', 'https://aparg.com/wp-content/uploads/2016/07/3.jpg', 'https://aparg.com/', '2021-05-14 17:09:07'),
(125, 'https://aparg.com/portfolio/vardhrat/', 'https://aparg.com/wp-content/uploads/2016/07/4.jpg', 'https://aparg.com/', '2021-05-14 17:09:07'),
(126, 'https://aparg.com/portfolio/vardhrat/', 'https://aparg.com/wp-content/uploads/2016/07/5.jpg', 'https://aparg.com/', '2021-05-14 17:09:08'),
(127, 'https://aparg.com/portfolio/vardhrat/', 'https://aparg.com/wp-content/uploads/2016/07/6.jpg', 'https://aparg.com/', '2021-05-14 17:09:08'),
(128, 'https://aparg.com/portfolio/vardhrat/', 'https://aparg.com/portfolio/aparg-smartad/', 'https://aparg.com/', '2021-05-14 17:09:09'),
(129, 'https://aparg.com/portfolio/carpark-app/', 'https://aparg.com/portfolio/kupi-nomer/', 'https://aparg.com/', '2021-05-14 17:09:14'),
(130, 'https://aparg.com/portfolio/aparg-smartad/', 'https://aparg.com/portfolio/asbalance/', 'https://aparg.com/', '2021-05-14 17:09:18'),
(131, 'https://aparg.com/portfolio/kupi-nomer/', 'https://aparg.com/portfolio/el-style-redesign/', 'https://aparg.com/', '2021-05-14 17:09:22'),
(132, 'https://aparg.com/portfolio/asbalance/', 'https://aparg.com/portfolio/velopark-app/', 'https://aparg.com/', '2021-05-14 17:09:26'),
(133, 'https://aparg.com/portfolio/el-style-redesign/', 'https://aparg.com/portfolio/drharutyunyan/', 'https://aparg.com/', '2021-05-14 17:09:29'),
(134, 'https://aparg.com/portfolio/velopark-app/', 'https://aparg.com/portfolio/proservice/', 'https://aparg.com/', '2021-05-14 17:09:32'),
(135, 'https://aparg.com/portfolio/drharutyunyan/', 'https://aparg.com/portfolio/lilithovhannisyan/', 'https://aparg.com/', '2021-05-14 17:09:35'),
(136, 'https://aparg.com/portfolio/proservice/', 'https://aparg.com/portfolio/medmarketing/', 'https://aparg.com/', '2021-05-14 17:09:41'),
(137, 'https://aparg.com/portfolio/lilithovhannisyan/', 'https://aparg.com/portfolio/eleonoraresort/', 'https://aparg.com/', '2021-05-14 17:09:44'),
(138, 'https://aparg.com/portfolio/medmarketing/', 'https://aparg.com/portfolio/hac/', 'https://aparg.com/', '2021-05-14 17:09:46'),
(139, 'https://aparg.com/portfolio/eleonoraresort/', 'https://aparg.com/portfolio/smartcity-system/', 'https://aparg.com/', '2021-05-14 17:09:50'),
(140, 'https://aparg.com/portfolio/hac/', 'https://aparg.com/portfolio/smartcity/', 'https://aparg.com/', '2021-05-14 17:09:54'),
(141, 'https://aparg.com/portfolio/smartcity-system/', 'https://aparg.com/portfolio/sirmed-app/', 'https://aparg.com/', '2021-05-14 17:09:57'),
(142, 'https://aparg.com/portfolio/smartcity/', 'https://aparg.com/portfolio/interactivetv/', 'https://aparg.com/', '2021-05-14 17:10:00'),
(143, 'https://aparg.com/portfolio/sirmed-app/', 'https://aparg.com/portfolio/gayanephotos/', 'https://aparg.com/', '2021-05-14 17:10:03'),
(144, 'https://aparg.com/portfolio/interactivetv/', 'https://aparg.com/portfolio/arpimed/', 'https://aparg.com/', '2021-05-14 17:10:06'),
(145, 'https://aparg.com/portfolio/gayanephotos/', 'https://aparg.com/portfolio/imtun/', 'https://aparg.com/', '2021-05-14 17:10:10'),
(146, 'https://aparg.com/portfolio/imtun/', 'https://aparg.com/portfolio/amelisstudio/', 'https://aparg.com/', '2021-05-14 17:10:15'),
(147, 'https://aparg.com/portfolio/amelisstudio/', 'https://aparg.com/portfolio/el-style/', 'https://aparg.com/', '2021-05-14 17:10:21'),
(148, 'https://aparg.com/portfolio/el-style/', 'https://aparg.com/portfolio/svarojich/', 'https://aparg.com/', '2021-05-14 17:10:27'),
(149, 'https://aparg.com/portfolio/awi/', 'https://aparg.com/portfolio/sirmed/', 'https://aparg.com/', '2021-05-14 17:10:31'),
(150, 'https://aparg.com/portfolio/svarojich/', 'https://aparg.com/portfolio/globus4u/', 'https://aparg.com/', '2021-05-14 17:10:34'),
(151, 'https://aparg.com/portfolio/sirmed/', 'https://aparg.com/portfolio/credit-corp/', 'https://aparg.com/', '2021-05-14 17:10:37'),
(152, 'https://aparg.com/portfolio/globus4u/', 'https://aparg.com/portfolio/yerevanresidence/', 'https://aparg.com/', '2021-05-14 17:10:40'),
(153, 'https://aparg.com/portfolio/credit-corp/', 'https://aparg.com/portfolio/armmix/', 'https://aparg.com/', '2021-05-14 17:10:44'),
(154, 'https://aparg.com/portfolio/yerevanresidence/', 'https://aparg.com/portfolio/workshopcafe-redesign/', 'https://aparg.com/', '2021-05-14 17:10:47'),
(155, 'https://aparg.com/portfolio/armmix/', 'https://aparg.com/portfolio/aparg-slider-plugin/', 'https://aparg.com/', '2021-05-14 17:10:52'),
(156, 'https://aparg.com/portfolio/workshopcafe-redesign/', 'https://aparg.com/portfolio/fioh-ngo/', 'https://aparg.com/', '2021-05-14 17:10:52'),
(157, 'https://aparg.com/portfolio/aparg-slider-plugin/', 'https://aparg.com/portfolio/sirkap/', 'https://aparg.com/', '2021-05-14 17:10:55'),
(158, 'https://aparg.com/portfolio/fioh-ngo/', 'https://aparg.com/portfolio/workshopcafe/', 'https://aparg.com/', '2021-05-14 17:10:57'),
(159, 'https://aparg.com/portfolio/sirkap/', 'https://aparg.com/portfolio/rheacutillo/', 'https://aparg.com/', '2021-05-14 17:11:01'),
(160, 'https://aparg.com/portfolio/workshopcafe/', 'https://aparg.com/portfolio/eivissa/', 'https://aparg.com/', '2021-05-14 17:11:03'),
(161, 'https://aparg.com/portfolio/rheacutillo/', 'https://aparg.com/portfolio/stellasabri/', 'https://aparg.com/', '2021-05-14 17:11:06'),
(162, 'https://aparg.com/portfolio/eivissa/', 'https://aparg.com/portfolio/avangardelos/', 'https://aparg.com/', '2021-05-14 17:11:09'),
(163, 'https://aparg.com/portfolio/stellasabri/', 'https://aparg.com/portfolio/partsforbmw/', 'https://aparg.com/', '2021-05-14 17:11:11'),
(164, 'https://aparg.com/portfolio/avangardelos/', 'https://aparg.com/portfolio/hotelhouse/', 'https://aparg.com/', '2021-05-14 17:11:12'),
(165, 'https://aparg.com/portfolio/partsforbmw/', 'https://aparg.com/portfolio/armecotravel/', 'https://aparg.com/', '2021-05-14 17:11:15'),
(166, 'https://aparg.com/portfolio/hotelhouse/', 'https://aparg.com/portfolio/ideaceramic/', 'https://aparg.com/', '2021-05-14 17:11:17'),
(167, 'https://aparg.com/portfolio/armecotravel/', 'https://aparg.com/portfolio/nvernissage/', 'https://aparg.com/', '2021-05-14 17:11:21'),
(168, 'https://aparg.com/portfolio/ideaceramic/', 'https://aparg.com/portfolio/stomland/', 'https://aparg.com/', '2021-05-14 17:11:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `link`
--
ALTER TABLE `link`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `link`
--
ALTER TABLE `link`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=169;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
