<?php 

require_once 'connection.php'; 

set_time_limit(0);

ob_implicit_flush();

class Bot {
  private $mysqli;
  private $arr = [];
  public function __construct($host, $db, $username, $password){
    $this->mysqli = new mysqli($host, $username, $password, $db);
  }

  public function index($url, $parent_url = null) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    
    $html = curl_exec($ch);

    $dom = new DOMDocument();
    @$dom->loadHTML($html);

    $links = $dom->getElementsByTagName('a');

    $content = $_POST['content'];

    foreach($links as $link){

      $urlparse = $link->getAttribute('href');

      $parsedParts = parse_url($urlparse);
      if(isset($parsedParts['host'])) {
        $fullUrl = $urlparse;
      } else {
        $parts = parse_url($url);
        $fullUrl = $parts['scheme'] . '://' . $parts['host'] . $urlparse;
      }
      
      if($this->isInternal($url, $urlparse)) {
        if(!(in_array($fullUrl, $this->arr))){
          
          $query = "INSERT INTO link (content, href, referrer, created_at) VALUES ('$content', '$fullUrl', '$parent_url', NOW())";
          mysqli_query($this->mysqli, $query);

          $this->arr[] = $fullUrl;
          $this->index($fullUrl, $url);
        }
      }
    }
  }

  public function find($url){
    $data = $this->findRecursive($url);
    $string = $this->drawRecoursive($data);
    echo $string;
    exit;
  }

  private function findRecursive($url, &$data = []) {
    $links = $this->mysqli->query("SELECT href FROM link WHERE referrer = '{$url}'");
    $links = $links->fetch_all(MYSQLI_ASSOC);
    $links = array_column($links, 'href');
    
    foreach($links as $link) {
      $a = ['url' => $link, 'links' => []];
      $this->findRecursive($link, $a['links']);
      $data[] = $a;
    }

    return $data;
  }
  
  private function drawRecoursive($data, &$string = '') {
    foreach($data as $item) {
      $string .= '<ul>';
      $string .= '<li>';
      $string .= $item['url'];
      $string .= $this->drawRecoursive($item['links'], $string);
      $string .= '</li>';
      $string .= '</ul>';
    }
    return $string;
  }

  private function isInternal($url, $link){
    $urlHost = parse_url($url, PHP_URL_HOST);
    $linkHost = parse_url($link, PHP_URL_HOST);
  
    $linkPath = parse_url($link, PHP_URL_PATH);
    $fragment = parse_url($link, PHP_URL_FRAGMENT);
    $hasSlesh = strlen($linkPath) && $linkPath[0] == '/';
    return (($linkHost == $urlHost) || ('www.' . $linkHost == $urlHost)) || (empty($linkHost) && $hasSlesh && !$fragment) ? true : false;
  }
  
}

?>



