<?php
  require_once 'bot.php';

  if(isset($_POST['submit']) && $_POST['submit'] ==='search' && isset($_POST['content']) && $_POST['content'] !== ''){
    $bot = new Bot('localhost', 'php-parser', 'root', 'root');
    $bot->index($_POST['content']);
  }

  if(isset($_POST['submit']) && $_POST['submit'] ==='find' && isset($_POST['content']) && $_POST['content'] !== ''){
      $bot = new Bot('localhost', 'php-parser', 'root', 'root');
      $bot->find($_POST['content']);
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="/css/style.css">
  <title>Document</title>
</head>
<body>
  <div class="input__search">
    <form action="index.php" class="form__input" method="post">
      <input type="text"  name="content" />
      <button type="submit" value="search" name="submit">Submit</button>
    </form>
  </div>
  <div class="input__search">
    <form action="index.php" class="form__input" method="post">
      <input class="find" name="content" type="text">
      <button value="find" type="submit" name="submit">Find</button>
    </form>
  </div>
</body>
</html>